/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : TIMER              
  File Name     : TIMER.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : 定时器测试实验                         
  Description  	: 短接蜂鸣器的跳线帽J8,当发生定时器中断时，蜂鸣器发出声音     
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include <NXP/iolpc2148.h>
#include <intrinsics.h>
#include "includes.h"


__irq __arm void IRQ_ISR_Handler (void);
void MM_TIMER1_ISR();
extern int OS_TimeMS;

//uc/os ii 在系统任务调度时用到了TIMEr0，所以不要更改TIMER0的任何配置

void Timer1_Init_US(INT32U US,void callback(void))
{
  VICIntSelect &= ~(1 << VIC_TIMER1);                         /* Enable interrupts                                        */
  VICVectAddr3  = (CPU_INT32U)callback;        /* Set the vector address                                   */
  VICVectCntl3  = 0x20 | VIC_TIMER1;                          /* Enable vectored interrupts                               */
  VICIntEnable  = (1 << VIC_TIMER1);                          /* Enable Interrupts                                        */

  T1IR  = 0xFF;//复位所有匹配和捕捉模式
  T1TCR = 0;    //禁止计数器1
  
  T1MR0         = 60*US;
  T1MCR         = 3;                                          /* Interrupt on MR0 (reset TC)                              */
  T1TCR         = 1;
}


/******************************************************************************
 * 函数名称: irq_handler
 * 入口参数: void
 * 返回参数: void
 *
 * 描述: IRQ exception handler, this will call appropriate isr after
 *       reading value out of VICVectAddr
 * 注意: This is ARM mode code - full 32 bit code	
 *****************************************************************************/
__irq __arm void irq_handler (void)
{
  void (*interrupt_function)();
  unsigned int vector;
  vector = VICVectAddr;                   // Get interrupt vector.
  interrupt_function = (void(*)())vector; // Call MM_TIMER0_ISR thru pointer
  (*interrupt_function)();  // Call vectored interrupt function
  VICVectAddr = 0;          // Clear interrupt in VIC
}

/*************************************************************************
 * 函数名称: fiq_handler
 * 入口参数: void
 * 返回参数: void
 *
 * 描述: FIQ subroutine
 * 注意: This is ARM mode code - full 32 bit code	
 *************************************************************************/
__fiq __arm void fiq_handler (void)
{
  while(1);
}

//定时器中断服务函数
void MM_TIMER1_ISR()
{
  static char count;
  static char BeepCount;
  if(count>=5)
  {
    count = 0;
    unsigned char second,i;
    char pt_tmp[20];
    LPC_Rtc_Time_t TM;
    LPC_Rtc_Date_t DT;
    
    RTC_GetDate(&DT);   
    RTC_GetTime(&TM);
    
    LCD12864_ClearSCR();           //清屏
    LCD12864_Circle(29,32,29,1);  //画表框
    for(i=0;i<12;i++)         
      LCD12864_Degree_Line(29,32,30*i,26,29,1);
    
    LCD12864_Degree_Line(29,32,30*TM.hour+(TM.minute/2),1,16,1); //画时针
    LCD12864_Degree_Line(29,32,6*TM.minute,1,22,1);                   //画分针
    LCD12864_Degree_Line(29,32,6*TM.second,1,26,1);                   //画秒针*/
    
    sprintf((char*)pt_tmp,"%d.%2d.%2d",DT.year,DT.month,DT.day);
    LCD12864_Puts(48,0,(char*)pt_tmp,1);                               //年月日
    
    sprintf((char*)pt_tmp,"%d",DT.DOW);
    LCD12864_Puts(94,24,(char*)pt_tmp,1);                              //星期*/
    
    sprintf((char*)pt_tmp,"%2d:%2d:%2d",TM.hour,TM.minute,TM.second);
    LCD12864_Puts(56,48,(char*)pt_tmp,1);                              //时间
    
    second = TM.second;
    if(TM.second == second) //等待
    {
      RTC_GetTime(&TM);
    }

    //OS_TimeMS ++;
  }
  
  //KeyDown();
  count++;
  if(BeepCount >= 3)
  {
    //BEEPOFF();
    BeepCount = 0;
  }
  
  T1IR = 1;                     // Clear timer interrupt
}