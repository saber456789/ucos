#ifndef _TIMER_H_
#define _TIMER_H_
#include "includes.h"

void Timer_Init(void);
__irq __arm void irq_handler (void);
__fiq __arm void fiq_handler (void);
void MM_TIMER0_ISR();
void Timer1_Init_US(INT32U US,void callback(void));
#define Timer1_Init_MS(MS,callback) Timer1_Init_US(MS*1000,callback)
#define Timer1_Init_S(S,callback) Timer1_Init_US(S*1000000,callback)
#endif