#include <NXP\iolpc2148.h>
#include "HC595.h"
#include "includes.h"
#define uchar unsigned char 
#define uint unsigned int
// A/D 通道选择命令字和工作寄存器
#define CHX     0x90    //通道Y+的选择控制字    
#define CHY     0xD0    //通道X+的选择控制字 

//定义管脚
#define SCK0   0x01<<4

static short JZ_X_START = 694;
static short JZ_Y_START = 3587;
static short JZ_X_END = 3658;
static short JZ_Y_END = 613;

//设置校验参数
void Set_JZStart(short sx,short sy,short ex,short ey)
{
  JZ_X_START = sx;
  JZ_Y_START = sy;
  JZ_X_END = ex;
  JZ_Y_END = ey;
}
//片选
void TCS2046_CS(char flag)
{
  if(flag)
  {
    HC595_DATA &= ~(1<<12);
    Write595();
  }
  else
  {
    HC595_DATA |= (1<<12);
    Write595();
  }
}
//初始化
void touch_Init(void)
{
  HC595_Init();
  TCS2046_CS(0);  
  
  IO0DIR |= SCK0;
  IO0SET |= SCK0;
}

//读ADC
static unsigned short RD_AD(void) 
{
    uint temp=0;
    uchar i;
    //touch_DIN=0;
    IO0SET |= SCK0;//touch_DCLK=1;
    for(i=0;i<12;i++) 
    {
        IO0CLR |= SCK0;//touch_DCLK=0;         
 	temp<<=1;
        if(IO0PIN&MISO) temp++;
        IO0SET |= SCK0;//touch_DCLK=1;
    }
    TCS2046_CS(0);//touch_CS=1;
    return(temp);
}

//读数据
uint read_trouch (unsigned char cmd) 
{
    unsigned char i;
    TCS2046_CS(0);//touch_CS=1;
    //touch_DIN=0;
    IO0CLR |= SCK0;//touch_DCLK=0;
    TCS2046_CS(1);//touch_CS=0;
    for(i=0;i<8;i++) 
    {
        if(cmd&0x80)  IO0SET |= MOSI; //touch_DIN=1;
        else IO0CLR |= MOSI;//touch_DIN=0;
        cmd = cmd<<1;
        IO0SET |= SCK0;//touch_DCLK=1;
        IO0CLR |= SCK0;//touch_DCLK=0;
    }
    return RD_AD();
}

void touch_GetAdXY(uint *x,uint *y) 
{
  *y= read_trouch(CHX); //横坐标
  *x= read_trouch(CHY); //纵坐标
}

unsigned short TPReadX(void)
{
  int x0 = read_trouch(CHY);
  //x0= ((int)x0 - (int)JZ_X_START)*200/(JZ_X_END - JZ_X_START) + 20;
  return x0;
}

unsigned short TPReadY(void)
{
  int y0 = read_trouch(CHX);
  //y0= -((int)y0 - (int)JZ_Y_END)*280/(JZ_Y_START - JZ_Y_END) + 300;
  return y0;
}
//计算坐标
void Get_XY(uint x0,uint y0,uint *x,uint *y)
{
  *y= -((int)y0 - (int)JZ_Y_END)*280/(JZ_Y_START - JZ_Y_END) + 300; //横坐标
  *x= ((int)x0 - (int)JZ_X_START)*200/(JZ_X_END - JZ_X_START) + 20; //纵坐标
}

/*
void    jiaozheng()
{
unsigned int x0,y0;
#if LCD_CALIBRATE == 1   
  unsigned int x1,y1,x2,y2,x3,y3;
#endif
  DispOneColor(WHITE); //全屏显示一种颜色
  
  DispSmallPic(148, 0, 92, 175, pic); //X坐标，Y坐标，图片X轴长度，图片Y轴长度，图片数组

#if LCD_CALIBRATE == 1 
  //左上角
  LCD_DrawLine(0,20,40,20,BLACK);
  LCD_DrawLine(20,0,20,40,BLACK);
  while(1)
  {
    touch_GetAdXY(&x0,&y0); //读取坐标位置
    if(y0 != 4095)  //有按键按下
    {
      LCD_DrawLine(0,20,40,20,WHITE);
      LCD_DrawLine(20,0,20,40,WHITE);
      break;
    }
  }
  Delayn(5000000);
  
  //右上角
  LCD_DrawLine(200,20,240,20,BLACK);
  LCD_DrawLine(220,0,220,40,BLACK);
  while(1)
  {
    touch_GetAdXY(&x1,&y1); //读取坐标位置
    if(y1 != 4095)  //有按键按下
    {
      LCD_DrawLine(200,20,240,20,WHITE);
      LCD_DrawLine(220,0,220,40,WHITE);
      break;
    }
  }
  Delayn(5000000);
  
  //左下角
  LCD_DrawLine(0,300,40,300,BLACK);
  LCD_DrawLine(20,280,20,320,BLACK);
  while(1)
  {
    touch_GetAdXY(&x2,&y2); //读取坐标位置
    if(y2 != 4095)  //有按键按下
    {
      LCD_DrawLine(0,300,40,300,WHITE);
      LCD_DrawLine(20,280,20,320,WHITE);
      break;
    }
  }
  Delayn(5000000);
  
  //右下角
  LCD_DrawLine(200,300,240,300,BLACK);
  LCD_DrawLine(220,280,220,320,BLACK);
  while(1)
  {
    touch_GetAdXY(&x3,&y3); //读取坐标位置
    if(y3 != 4095)  //有按键按下
    {
      LCD_DrawLine(200,300,240,300,WHITE);
      LCD_DrawLine(220,280,220,320,WHITE);
      break;
    }
  }
  Delayn(5000000);
  
  Set_JZStart((x0+x2)/2,(y0+y1)/2,(x1+x3)/2,(y2+y3)/2);
#endif

}
*/