#ifndef _TCS2046_H_
#define _TCS2046_H_

#define uint unsigned int

void touch_Init(void);
void touch_GetAdXY(uint *x,uint *y);
void Set_JZStart(short sx,short sy,short ex,short ey);
void Get_XY(uint x0,uint y0,uint *x,uint *y);
unsigned short TPReadX(void);
unsigned short TPReadY(void);
void    jiaozheng();
#endif