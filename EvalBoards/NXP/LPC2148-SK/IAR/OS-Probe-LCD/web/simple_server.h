#ifndef _SIMPLE_SERVER_H_
#define _SIMPLE_SERVER_H_

#define BUFFER_SIZE 1500         //400

extern uint8_t mymac[6];
extern uint8_t myip[4];
extern char baseurl[];
extern uint16_t mywwwport;
extern char password[];
extern uint16_t myudpport;

extern void delay_ms(unsigned char ms);

#endif