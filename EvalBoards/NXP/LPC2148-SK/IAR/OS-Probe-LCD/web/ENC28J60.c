/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : ENC28J60              
  File Name     : ENC28J60.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : WAB网页服务器实验                        
  Description  	: 无           
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include <NXP\iolpc2148.h>
#include "system.h"
#include "rs232.h"


//函数声明
extern int simple_server(void);

//主函数
/*
int main(void)
{
  FrecInit();             //初始化系统时钟
  UART0Initialize(115200);  //初始化串口
  simple_server();        //开始网络服务
}
*/