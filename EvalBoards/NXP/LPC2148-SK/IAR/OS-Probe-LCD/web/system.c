//system.c
#include <NXP\iolpc2148.h>
#include "system.h"
#include "rs232.h"



//oscillator frequency
//IMPORTANT - if you use oscillator with different frequency,
//please change this value, bec鄒se timer not work correctly


unsigned int GetCclk(void)
{
  //return real processor clock speed
  return OSCILLATOR_CLOCK_FREQUENCY * (PLLCON & 1 ? (PLLCFG & 0xF) + 1 : 1);
}

unsigned int GetPclk(void)
{
  //VPBDIV - determines the relationship between the processor clock (cclk)
  //and the clock used by peripheral devices (pclk).
  unsigned int divider;
  switch (VPBDIV & 3)
    {
      case 0: divider = 4;  break;
      case 1: divider = 1;  break;
      case 2: divider = 2;  break;
    }
  return GetCclk() / divider;
}
/*
*PLL模块初始化
*存储加速模块初始化
*/

void FrecInit(void)
{
    /* 设置系统各部分时钟 */
       PLLCON = 1;
    #if ((Fcclk / 4) / Fpclk) == 1
            VPBDIV = 0;
    #endif
    #if ((Fcclk / 4) / Fpclk) == 2
            VPBDIV = 2;
    #endif
    #if ((Fcclk / 4) / Fpclk) == 4
            VPBDIV = 1;
    #endif
    #if (Fcco / Fcclk) == 2
            PLLCFG = ((Fcclk / Fosc) - 1) | (0 << 5);
    #endif
    #if (Fcco / Fcclk) == 4
            PLLCFG = ((Fcclk / Fosc) - 1) | (1 << 5);
    #endif
    #if (Fcco / Fcclk) == 8
            PLLCFG = ((Fcclk / Fosc) - 1) | (2 << 5);
    #endif
    #if (Fcco / Fcclk) == 16
            PLLCFG = ((Fcclk / Fosc) - 1) | (3 << 5);
    #endif
            PLLFEED = 0xaa;
            PLLFEED = 0x55;
            while((PLLSTAT & (1 << 10)) == 0);
            PLLCON = 3;
            PLLFEED = 0xaa;
            PLLFEED = 0x55;	

// Memory map init flash memory is maped on 0 address
        MEMMAP_bit.MAP = 1;
}





