#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//timer.h

// Initialize processor speed
void FrecInit(void);
// Get cclk
unsigned int GetCclk(void);

//系统时钟
#define Fosc        12000000UL        //晶振时钟 【Hz】
#define Fcclk       (Fosc * 5)        //系统频率，必须为Fosc的整数倍(1~32)，且<=60MHZ
#define Fcco	    (Fcclk * 4)	      //CCO频率，必须为Fcclk的2、4、8、16倍，范围为156MHz~320MHz
#define Fpclk       (Fcclk / 4) * 4   //VPB时钟频率，只能为(Fcclk / 4)的1 ~ 4倍


#define VIC_TIMER0_bit (1 << VIC_TIMER0)

#endif