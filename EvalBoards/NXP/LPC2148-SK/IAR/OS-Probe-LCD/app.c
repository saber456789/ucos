/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2003-2008; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                             EXAMPLE CODE
*
*                                              NXP LPC2148
*                                                on the
*                                     IAR LPC2148-SK Evaluation Board
*
* Filename      : app.c
* Version       : V1.01
* Programmer(s) : JJL
:                 FT
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>


/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/

#define  APP_USERIF_STATE_MAX   5

/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

/* ----------------- APPLICATION GLOBALS ------------------ */
static  OS_STK          App_TaskStartStk[APP_CFG_TASK_START_STK_SIZE];

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  OS_STK          App_TaskUserIFStk[APP_CFG_TASK_USER_IF_STK_SIZE];
static  OS_STK          App_TaskKbdStk[APP_CFG_TASK_KBD_STK_SIZE];

static  OS_EVENT       *App_UserIF_Mbox;

static  CPU_CHAR        App_UserIFLine1[17];
static  CPU_CHAR        App_UserIFLine2[17];

static  CPU_INT32U      App_B1Cnts;
static  CPU_INT32U      App_B2Cnts;
static  CPU_INT32U      App_ADC;
#endif

                                                                /* -------------- uC/PROBE RELATED GLOBALS ---------------- */
#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) && \
    (APP_CFG_PROBE_COM_EN       == DEF_ENABLED) && \
    (PROBE_COM_CFG_STAT_EN      == DEF_ENABLED)
static  CPU_FP32        App_ProbeComRxPktSpd;
static  CPU_FP32        App_ProbeComTxPktSpd;
static  CPU_FP32        App_ProbeComTxSymSpd;
static  CPU_FP32        App_ProbeComTxSymByteSpd;

static  CPU_INT32U      App_ProbeComRxPktLast;
static  CPU_INT32U      App_ProbeComTxPktLast;
static  CPU_INT32U      App_ProbeComTxSymLast;
static  CPU_INT32U      App_ProbeComTxSymByteLast;

static  CPU_INT32U      App_ProbeComCtrLast;
#endif

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED)
volatile  CPU_INT32U    App_ProbeCnts;
volatile  CPU_BOOLEAN   App_ProbeB1;
volatile  CPU_BOOLEAN   App_ProbeB2;
volatile  CPU_INT16U    App_ProbeADC;
#endif

/*
*********************************************************************************************************
*                                         LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  App_EventCreate            (void);
static  void  App_TaskStart              (void        *p_arg);
static  void  App_TaskCreate             (void);

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_TaskUserIF             (void        *p_arg);
static  void  App_TaskKbd                (void        *p_arg);

static  void  App_DispScr_SignOn         (void);
static  void  App_DispScr_VerTickRate    (void);
static  void  App_DispScr_CPU            (void);
static  void  App_DispScr_CtxSw          (void);
static  void  App_DispScr_Inputs         (void);
#endif

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) || \
    (APP_CFG_PROBE_COM_EN       == DEF_ENABLED)
static  void  App_ProbeInit              (void);
#endif

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED)
static  void  App_ProbeCallback          (void);
#endif

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_FormatDec             (CPU_INT08U  *s,
                                         CPU_INT32U   value,
                                         CPU_INT08U   digits);

#endif
/*
*********************************************************************************************************
*                                    LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Argument(s) : none
*
* Return(s)   : none
*********************************************************************************************************
*/
#define TICKS_PER_SECOND 10       // TIMER0 interrupt is 10Hz
static  OS_STK          App_TaskStartStk[APP_CFG_TASK_START_STK_SIZE];
static  OS_STK          Task1Stk[APP_CFG_TASK_START_STK_SIZE];
static  OS_STK          Task2Stk[APP_CFG_TASK_START_STK_SIZE];
OS_EVENT *ResourceMutex;
void  main (void)
{

    CPU_INT08U  err;

    BSP_IntDisAll();                                            /* Disable all interrupts until we are ready to accept them */
    
    OSInit();                                                   /* Initialize "uC/OS-II, The Real-Time Kernel"              */

    OSTaskCreateExt((void (*)(void *)) App_TaskStart,           /* Create the start task                                    */
                    (void           *) 0,
                    (OS_STK         *)&App_TaskStartStk[APP_CFG_TASK_START_STK_SIZE - 1],
                    (INT8U           ) APP_CFG_TASK_START_PRIO,
                    (INT16U          ) APP_CFG_TASK_START_PRIO,
                    (OS_STK         *)&App_TaskStartStk[0],
                    (INT32U          ) APP_CFG_TASK_START_STK_SIZE,
                    (void           *)0,
                    (INT16U          )(OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));

#if (OS_TASK_NAME_SIZE > 11)
    OSTaskNameSet(APP_CFG_TASK_START_PRIO, "Start Task", &err);
#endif

    OSStart();                                                  /* Start multitasking (i.e. give control to uC/OS-II)       */
}


/*
*********************************************************************************************************
*                                          AppTaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Note(s)     : (1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                   used.  The compiler should not generate any code for this statement.
*
*               (2) Interrupts are enabled once the task starts because the I-bit of the CCR register was
*                   set to 0 by 'OSTaskCreate()'.
*********************************************************************************************************
*/

static  void  App_TaskStart (void *p_arg)
{
    (void)p_arg;
    INT8U err; 
    BSP_Init();                                                 /* Initialize BSP functions                                 */                                                                 //初始化定时器0
        
#if (OS_TASK_STAT_EN > 0)
    OSStatInit();                                               /* Determine CPU capacity                                   */
#endif

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) || \
    (APP_CFG_PROBE_COM_EN       == DEF_ENABLED)
    App_ProbeInit();
#endif
        
    ResourceMutex = OSMutexCreate(4, &err);
    
    App_TaskCreate( );                                           /* Create application tasks                                 */

    while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.           */
        OSTimeDlyHMSM(0, 0, 0, 10);
        
        OSMutexPend(ResourceMutex, 0, &err);
        sendStr(0,"main task\n");
        OSMutexPost(ResourceMutex);
    }
}


/*
*********************************************************************************************************
*                                      App_EventCreate()
*
* Description :  Create the application events.
*
* Argument(s) :  none.
*
* Return(s)   :  none.
*********************************************************************************************************
*/

static  void  App_EventCreate (void)
{
#if (APP_CFG_LCD_EN     == DEF_ENABLED) && \
    (OS_EVENT_NAME_SIZE  > 8)    
    CPU_INT08U err;
#endif    
    
    
#if (APP_CFG_LCD_EN == DEF_ENABLED)
    App_UserIF_Mbox = OSMboxCreate((void *)0);                  /* Create MBOX for communication between Kbd and UserIF     */

#if (OS_EVENT_NAME_SIZE > 8)
    OSEventNameSet(App_UserIF_Mbox, (CPU_INT08U *)"User I/F", &err);
#endif    
#endif    
}

static  void  Task1 (void *p_arg)
{
  INT8U err; 
  OSTimeDlyHMSM(0, 0, 0, 30);
  OSMutexPend(ResourceMutex, 0, &err);
  sendStr(0,"task 1\n");
  OSMutexPost(ResourceMutex);
}

/*任务2*/
static  void  Task2 (void *p_arg)
{
  INT8U err; 
  for(;;)
  {
    OSTimeDlyHMSM(0, 0, 0, 30);
    
    OSMutexPend(ResourceMutex, 0, &err);
    sendStr(0,"task2\n");
    OSMutexPost(ResourceMutex);
  }
}


/*
*********************************************************************************************************
*                                      AppTaskCreate()
*
* Description :  Create the application tasks.
*
* Argument(s) :  none.
*
* Return(s)   :  none.
*********************************************************************************************************
*/

static  void  App_TaskCreate (void)
{
    OSTaskCreateExt((void (*)(void *)) Task1,
                    (void           *) 0,
                    (OS_STK         *)&Task1Stk[APP_CFG_TASK_START_STK_SIZE - 1],
                    (INT8U           ) TASK_PRIO1,
                    (INT16U          ) TASK_PRIO1,
                    (OS_STK         *)&Task1Stk[0],
                    (INT32U          ) APP_CFG_TASK_START_STK_SIZE,
                    (void           *) 0,
                    (INT16U          )(OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
    
    OSTaskCreateExt((void (*)(void *)) Task2,
                    (void           *) 0,
                    (OS_STK         *)&Task2Stk[APP_CFG_TASK_START_STK_SIZE - 1],
                    (INT8U           ) TASK_PRIO2,
                    (INT16U          ) TASK_PRIO2,
                    (OS_STK         *)&Task2Stk[0],
                    (INT32U          ) APP_CFG_TASK_START_STK_SIZE,
                    (void           *) 0,
                    (INT16U          )(OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));


}


/*
*********************************************************************************************************
*                                    App_TaskKbd()
*
* Description : Monitor the state of the push buttons and passes messages to AppTaskUserIF()
*
* Argument(s) : p_arg       Argument passed to 'AppTaskKbd()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Note(s)     : (1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                   used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_TaskKbd (void *p_arg)
{
    CPU_BOOLEAN  b1;                                            /* State of Push Button #1                        */
    CPU_BOOLEAN  b1_prev;
    CPU_BOOLEAN  b2;                                            /* State of Push Button #2                        */
    CPU_BOOLEAN  b2_prev;
    CPU_INT08U   key;


    (void)p_arg;

    key      = 1;
    b1_prev = DEF_FALSE;
    b2_prev = DEF_FALSE;

    while (DEF_TRUE) {

        b1 = BSP_PB_GetStatus(1);
        b2 = BSP_PB_GetStatus(2);

        if (b1 == DEF_TRUE && b1_prev == DEF_FALSE) {
            App_B1Cnts++;

            if (key == APP_USERIF_STATE_MAX) {
                key = 1;
            } else {
                key++;
            }
            OSMboxPost(App_UserIF_Mbox, (void *)key);
        }

        if (b2 == DEF_TRUE && b2_prev == DEF_FALSE) {
            App_B2Cnts++;

            BSP_LED_Toggle(1);
        }

        b1_prev = b1;
        b2_prev = b2;

        OSTimeDlyHMSM(0, 0, 0, 50);
    }
}
#endif


/*
*********************************************************************************************************
*                                    AppTaskUserIF()
*
* Description : Updates LCD.
*
* Argument(s) : p_arg       Argument passed to 'AppTaskUserIF()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Note(s)     : (1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                   used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_TaskUserIF (void *p_arg)
{
    CPU_INT32U   pstate;
    CPU_INT32U   nstate;
    CPU_INT08U  *msg;
    CPU_INT08U   err;


    (void)p_arg;

    DispInit(2, 16);                                            /* Initialize the LCD module                            */

    BSP_LED_On(1);                                              /* Turn ON    the LCD Backlight                         */

    App_DispScr_SignOn();                                        /* Display 'sign on' message                            */
    OSTimeDlyHMSM(0, 0, 1, 0);
    pstate = 1;
    nstate = 1;


    while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.       */
        msg = (CPU_INT08U *)(OSMboxPend(App_UserIF_Mbox, OS_TICKS_PER_SEC / 3, &err));
        if (err == OS_NO_ERR) {
            pstate = (CPU_INT32U)msg;
        }

        if (pstate != nstate) {
            nstate = pstate;
            DispClrScr();
        }

        switch (pstate) {
            case 2:
                 App_DispScr_VerTickRate();
                 break;

            case 3:
                 App_DispScr_CPU();
                 break;

            case 4:
                 App_DispScr_CtxSw();
                 break;

            case 5:
                 App_DispScr_Inputs();
                 break;

            case 1:
            default:
                 App_DispScr_SignOn();
                 break;
        }
    }
}
#endif


/*
*********************************************************************************************************
*                                            DISPLAY SCREENS
*
* Descrition  :  Display one of the screens used in the demonstration.
*
* Argument(s) :  none.
*
* Return(s)   :  none.
*********************************************************************************************************
*/

#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_DispScr_SignOn (void)
{
    Str_Copy(App_UserIFLine1, "Micrium uC/OS-II");
    Str_Copy(App_UserIFLine2, "NXP      LPC2148");

    DispStr(0, 0, App_UserIFLine1);
    DispStr(1, 0, App_UserIFLine2);
}

static  void  App_DispScr_VerTickRate (void)
{
    CPU_INT32U  value;


    Str_Copy(App_UserIFLine1, "uC/OS-II:  Vx.yy");
    value               = (CPU_INT32U)OSVersion();
    App_UserIFLine1[12] =  value / 100       + '0';
    App_UserIFLine1[14] = (value % 100) / 10 + '0';
    App_UserIFLine1[15] = (value %  10)      + '0';

    Str_Copy(App_UserIFLine2, "TickRate:   xxxx");
    value = (CPU_INT32U)OS_TICKS_PER_SEC;
    App_FormatDec(&App_UserIFLine2[12], value, 4);

    DispStr(0, 0, App_UserIFLine1);
    DispStr(1, 0, App_UserIFLine2);
}

static  void  App_DispScr_CPU (void)
{
    CPU_INT32U  value;


    Str_Copy(App_UserIFLine1, "CPU Usage:xx %  ");
    value                = (CPU_INT32U)OSCPUUsage;
    App_UserIFLine1[10]  = (value / 10) + '0';
    App_UserIFLine1[11]  = (value % 10) + '0';

    Str_Copy(App_UserIFLine2, "CPU Speed:xx MHz");
    value                = (CPU_INT32U)BSP_CPU_ClkFreq() / 1000000L;
    App_UserIFLine2[10]  = (value / 10) + '0';
    App_UserIFLine2[11]  = (value % 10) + '0';

    DispStr(0, 0, App_UserIFLine1);
    DispStr(1, 0, App_UserIFLine2);
}

static  void  App_DispScr_CtxSw (void)
{
    CPU_INT32U  value;


    Str_Copy(App_UserIFLine1, "#Ticks: xxxxxxxx");
    value = (CPU_INT32U)OSTime;
    App_FormatDec(App_UserIFLine1 + 8, value, 8);

    Str_Copy(App_UserIFLine2, "#CtxSw: xxxxxxxx");
    value = (CPU_INT32U)OSCtxSwCtr;
    App_FormatDec(App_UserIFLine2 + 8, value, 8);

    DispStr(0, 0, App_UserIFLine1);
    DispStr(1, 0, App_UserIFLine2);
}

static  void  App_DispScr_Inputs (void)
{
    Str_Copy(App_UserIFLine1, "    ADC: xxxx   ");
    Str_Copy(App_UserIFLine2, "B1: yyy  B2: zzz");

    App_FormatDec(&App_UserIFLine1[9],  App_ADC,      4);
    App_FormatDec(&App_UserIFLine2[4],  App_B1Cnts, 3);
    App_FormatDec(&App_UserIFLine2[13], App_B2Cnts, 3);

    DispStr(0, 0, App_UserIFLine1);
    DispStr(1, 0, App_UserIFLine2);
}
#endif


/*
*********************************************************************************************************
*                                         App_ProbeInit()
*
* Description : Initializes uC/Probe Module
*
* Argument(s) : p_arg       Argument passed to 'AppTaskProbeStr()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Note(s)     : (1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                   used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) || \
    (APP_CFG_PROBE_COM_EN       == DEF_ENABLED)
static  void  App_ProbeInit()
{
#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED)
    (void)App_ProbeCnts;
    (void)App_ProbeB1;
    (void)App_ProbeB2;
    (void)App_ProbeADC;

#if (APP_CFG_PROBE_COM_EN  == DEF_ENABLED) && \
    (PROBE_COM_CFG_STAT_EN == DEF_ENABLED)
    (void)App_ProbeComRxPktSpd;
    (void)App_ProbeComTxPktSpd;
    (void)App_ProbeComTxSymSpd;
    (void)App_ProbeComTxSymByteSpd;
#endif

    OSProbe_Init();
    OSProbe_SetCallback(App_ProbeCallback);
    OSProbe_SetDelay(50);
#endif

#if (APP_CFG_PROBE_COM_EN  == DEF_ENABLED)
    ProbeCom_Init();                                            /* Initialize the uC/Probe communications module            */

#if (APP_CFG_KSD_EN == DEF_ENABLED)
    KSD_Init();
#endif    
    
#if (PROBE_COM_CFG_RS232_EN == DEF_ENABLED)
    ProbeRS232_Init(115200);
    ProbeRS232_RxIntEn();
#endif
#endif    
}
#endif
                     
/*
*********************************************************************************************************
*                                         App_ProbeCallback()
*
* Description : This function is called by the uC/Probe uC/OS-II plug-in after updating task information.
*
* Argument(s) : none.
*
* Return(s)   : none.
*********************************************************************************************************
*/

#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED)
static  void  App_ProbeCallback (void)
{
#if (APP_CFG_PROBE_COM_EN  == DEF_ENABLED) && \
    (PROBE_COM_CFG_STAT_EN == DEF_ENABLED)
    CPU_INT32U  ctr_curr;
    CPU_INT32U  rxpkt_curr;
    CPU_INT32U  txpkt_curr;
    CPU_INT32U  sym_curr;
    CPU_INT32U  symbyte_curr;
#endif

    
    App_ProbeCnts++;
    App_ProbeB1  = BSP_PB_GetStatus(1);
    App_ProbeB2  = BSP_PB_GetStatus(2);
    App_ProbeADC = (BSP_ADC_GetStatus(1) * 100) / 1024;

    
#if (APP_CFG_PROBE_COM_EN  == DEF_ENABLED) && \
    (PROBE_COM_CFG_STAT_EN == DEF_ENABLED)
    ctr_curr     = OSTime;
    rxpkt_curr   = ProbeCom_RxPktCtr;
    txpkt_curr   = ProbeCom_TxPktCtr;
    sym_curr     = ProbeCom_TxSymCtr;
    symbyte_curr = ProbeCom_TxSymByteCtr;

    if ((ctr_curr - App_ProbeComCtrLast) >= OS_TICKS_PER_SEC) {

        App_ProbeComRxPktSpd      = ((CPU_FP32)(rxpkt_curr   - App_ProbeComRxPktLast)     / (ctr_curr - App_ProbeComCtrLast)) * OS_TICKS_PER_SEC;
        App_ProbeComTxPktSpd      = ((CPU_FP32)(txpkt_curr   - App_ProbeComTxPktLast)     / (ctr_curr - App_ProbeComCtrLast)) * OS_TICKS_PER_SEC;
        App_ProbeComTxSymSpd      = ((CPU_FP32)(sym_curr     - App_ProbeComTxSymLast)     / (ctr_curr - App_ProbeComCtrLast)) * OS_TICKS_PER_SEC;
        App_ProbeComTxSymByteSpd  = ((CPU_FP32)(symbyte_curr - App_ProbeComTxSymByteLast) / (ctr_curr - App_ProbeComCtrLast)) * OS_TICKS_PER_SEC;

        App_ProbeComCtrLast       = ctr_curr;
        App_ProbeComRxPktLast     = rxpkt_curr;
        App_ProbeComTxPktLast     = txpkt_curr;
        App_ProbeComTxSymLast     = sym_curr;
        App_ProbeComTxSymByteLast = symbyte_curr;
    }
#endif
}
#endif


/*
*********************************************************************************************************
*                                      App_FormatDec()
*
* Description : Convert a decimal value to ASCII (with leading zeros).
*
* Argument(s) : s           Pointer to the destination ASCII string.
*               value       Value to convert (assumes an unsigned value).
*               digits      The desired number of digits.
*
* Return(s)   : none.
*********************************************************************************************************
*/
#if (APP_CFG_LCD_EN == DEF_ENABLED)
static  void  App_FormatDec (CPU_INT08U *s, 
                             CPU_INT32U  value, 
                             CPU_INT08U  digits)
{
    CPU_INT08U  i;
    CPU_INT32U  mult;


    mult = 1;
    for (i = 0; i < (digits - 1); i++) {
        mult *= 10;
    }
    while (mult > 0) {
        *s++   = value / mult + '0';
        value %= mult;
        mult  /= 10;
    }
}
#endif

/*
*********************************************************************************************************
*********************************************************************************************************
**                                         uC/OS-II APP HOOKS
*********************************************************************************************************
*********************************************************************************************************
*/

#if (OS_APP_HOOKS_EN > 0)
/*
*********************************************************************************************************
*                                      TASK CREATION HOOK (APPLICATION)
*
* Description : This function is called when a task is created.
*
* Argument(s) : ptcb   is a pointer to the task control block of the task being created.
*
* Note(s)     : (1) Interrupts are disabled during this call.
*********************************************************************************************************
*/

void  App_TaskCreateHook (OS_TCB *ptcb)
{
#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) && (OS_PROBE_HOOKS_EN > 0)
    OSProbe_TaskCreateHook(ptcb);
#endif
}

/*
*********************************************************************************************************
*                                    TASK DELETION HOOK (APPLICATION)
*
* Description : This function is called when a task is deleted.
*
* Argument(s) : ptcb   is a pointer to the task control block of the task being deleted.
*
* Note(s)     : (1) Interrupts are disabled during this call.
*********************************************************************************************************
*/

void  App_TaskDelHook (OS_TCB *ptcb)
{
    (void)ptcb;
}

/*
*********************************************************************************************************
*                                      IDLE TASK HOOK (APPLICATION)
*
* Description : This function is called by OSTaskIdleHook(), which is called by the idle task.  This hook
*               has been added to allow you to do such things as STOP the CPU to conserve power.
*
* Argument(s) : none.
*
* Note(s)     : (1) Interrupts are enabled during this call.
*********************************************************************************************************
*/

#if OS_VERSION >= 251
void  App_TaskIdleHook (void)
{
}
#endif

/*
*********************************************************************************************************
*                                        STATISTIC TASK HOOK (APPLICATION)
*
* Description : This function is called by OSTaskStatHook(), which is called every second by uC/OS-II's
*               statistics task.  This allows your application to add functionality to the statistics task.
*
* Argument(s) : none.
*********************************************************************************************************
*/

void  App_TaskStatHook (void)
{
}

/*
*********************************************************************************************************
*                                        TASK SWITCH HOOK (APPLICATION)
*
* Description : This function is called when a task switch is performed.  This allows you to perform other
*               operations during a context switch.
*
* Argument(s) : none.
*
* Note(s)     : (1) Interrupts are disabled during this call.
*
*               (2) It is assumed that the global pointer 'OSTCBHighRdy' points to the TCB of the task that
*                   will be 'switched in' (i.e. the highest priority task) and, 'OSTCBCur' points to the
*                  task being switched out (i.e. the preempted task).
*********************************************************************************************************
*/

#if OS_TASK_SW_HOOK_EN > 0
void  App_TaskSwHook (void)
{
#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) && (OS_PROBE_HOOKS_EN > 0)
    OSProbe_TaskSwHook();
#endif
}
#endif

/*
*********************************************************************************************************
*                                     OS_TCBInit() HOOK (APPLICATION)
*
* Description : This function is called by OSTCBInitHook(), which is called by OS_TCBInit() after setting
*               up most of the TCB.
*
* Argument(s) : ptcb    is a pointer to the TCB of the task being created.
*
* Note(s)     : (1) Interrupts may or may not be ENABLED during this call.
*********************************************************************************************************
*/

#if OS_VERSION >= 204
void  App_TCBInitHook (OS_TCB *ptcb)
{
    (void)ptcb;
}
#endif

/*
*********************************************************************************************************
*                                        TICK HOOK (APPLICATION)
*
* Description : This function is called every tick.
*
* Argument(s) : none.
*
* Note(s)     : (1) Interrupts may or may not be ENABLED during this call.
*********************************************************************************************************
*/

#if OS_TIME_TICK_HOOK_EN > 0
void  App_TimeTickHook (void)
{
#if (APP_CFG_PROBE_OS_PLUGIN_EN == DEF_ENABLED) && (OS_PROBE_HOOKS_EN > 0)
    OSProbe_TickHook();
#endif
}
#endif
#endif