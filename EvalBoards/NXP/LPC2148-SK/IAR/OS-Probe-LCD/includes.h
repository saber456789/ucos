/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2003-2006; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                         MASTER INCLUDE FILE
*
*                                              NXP LPC2148
*                                                on the
*                                     IAR LPC2148-SK Evaluation Board
*
* Filename      : includes.h
* Version       : V1.01
* Programmer(s) : JJL
*                 FT
*********************************************************************************************************
*/

#ifndef __INCLUDES_H__
#define __INCLUDES_H__

#include    <stdio.h>
#include    <string.h>
#include    <ctype.h>
#include    <stdlib.h>
#include    <stdarg.h>

#include    <iolpc2148.h>

#include    <ucos_ii.h>

#include    <cpu.h>
#include    <lib_def.h>
#include    <lib_mem.h>
#include    <lib_str.h>

#include "HC595.h"
#include "pic.h"
#include "TCS2046.h"
#include "TFT_LCD.h"
#include "system.h"
#include "rs232.h"
#include "GUI.h"
#include "uart.h"
#include "GUIDEMO.h"
#include "LPC_Rtc.h"
#include "key.h"
#include "beep.h"
#include    "Timer.h"
#include    "LCD12864_CONFIG.H"
#include    "LCD_Driver.h"
#include    "LCD12864_BASIC.H"
#include    "LCD12864_STOCKC.H"
#include    "FONT5_7.H"
#include    "FONT8_8.H"
#include    "FONT24_32.H"
#include    "LOADBIT.H"
#include    "WINDOWS.H"
#include    "LCD12864_MENU.H"
#include    "SPLINE.H"
#include    "pll.h"
#include    "Chinese_Character.h"
#include    "config.h"
#include    "enc28j60.h"
#include    "simple_server.h"
#include    "ip_arp_udp_tcp.h"
#include    "IAP.h"
#include    "iic.h"
#include    <app_cfg.h>

#if (APP_CFG_LCD_EN == DEF_ENABLED)
#include    <LCD.h>
#endif

#include    <bsp.h>

#if (APP_CFG_PROBE_OS_PLUGIN_EN  == DEF_ENABLED)
#include    <os_probe.h>
#endif

#if (APP_CFG_PROBE_COM_EN == DEF_ENABLED)
#include    <probe_com.h>

#if (PROBE_COM_CFG_RS232_EN == DEF_ENABLED)
#include    <probe_rs232.h>
#endif
#endif

#if (APP_CFG_KSD_EN == DEF_ENABLED)
#include    <ksd.h>
#endif



#endif




