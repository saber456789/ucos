/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : TFT_LCD              
  File Name     : TFT_LCD.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : 实现液晶屏驱动功能                         
  Description  	: 无                 
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include <NXP\IOLPC2148.h>
#include "hc595.h"
#include "includes.h"
#include "LCD_ConfDefaults.h"
#include "lcd.h"
#include "GUI.h"
#include "GUI_Protected.h"
#define LCD_COLORINDEX BLUE
#define U16 unsigned short
#define U8 unsigned char

#define CH375DataOut() IO1DIR |= (0XFF<<16) //D0~D7
#define CH375DataIn() IO1DIR &= ~(0XFF<<16)   //D0~D7
#define LCD_RS_H() IO0SET_bit.P0_30 = 1
#define LCD_RS_L() IO0CLR_bit.P0_30 = 1
#define LCD_WR_H() IO0SET_bit.P0_21 = 1
#define LCD_WR_L() IO0CLR_bit.P0_21 = 1
#define LCD_RD_H() IO0SET_bit.P0_20 = 1
#define LCD_RD_L() IO0CLR_bit.P0_20 = 1
#define LCD_CS_L() IO0CLR_bit.P0_17 = 1
#define LCD_CS_H() IO0SET_bit.P0_17 = 1
#define LCD_RESET_L() HC595_DATA &= ~(1<<7);Write595()
#define LCD_RESET_H() HC595_DATA |= (1<<7) ;Write595()

//函数声明
void LCD_Port(void);
void TftInit(void);
void DispOneColor(unsigned short Color);
void TFTWriCom(unsigned short ch);
void DispSmallPic(unsigned short x, unsigned short y, unsigned short w, unsigned short h, const unsigned char *str);
void DispPic240_320(const unsigned char *str);

static void  DrawBitLine1BPP(int x, int y, U8 const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans);
static void  DrawBitLine2BPP(int x, int y, U8 const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans);
static void  DrawBitLine4BPP(int x, int y, U8 const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans);
void DrawBitLine8BPP(int x, int y, U8 const*p, int xsize, const LCD_PIXELINDEX*pTrans);
void DrawBitLine16BPP(int x, int y, U16 const*p, int xsize);
//初始化液晶接口
void LCD_Port(void)
{  
  IO0SET_bit.P0_21 = 1; //WR
  IO0SET_bit.P0_20 = 1; //RD
  
  IO0DIR_bit.P0_20 = 1; //RD
  IO0DIR_bit.P0_21 = 1; //WR
  IO0DIR_bit.P0_30 = 1; //RS
  IO0DIR_bit.P0_17 = 1;  //CS
  
  IO0DIR_bit.P0_11 = 1;
  IO0DIR_bit.P0_12 = 1;
  
  HC595_Init();
}

//延时
void DelayNS(unsigned short k)
{
    unsigned short i;
    unsigned int j;
    for(i = k;i > 0;i--)
        for(j = 1000;j > 0;j--);    
}

//向HC573写入数据
void Write_HC573(unsigned short Dat)
{
  unsigned char dat;
  dat = (unsigned char)(Dat&0xFF);
  IO1SET |= (((unsigned int)dat)<<16);
  IO1CLR |= ((~(((unsigned int)dat)<<16))&((0XFF<<16)));
  IO0SET_bit.P0_11 = 1;IO0CLR_bit.P0_11 = 1;

  dat = (unsigned char)((Dat>>8)&0xFF);
  IO1SET |= (((unsigned int)dat)<<16);
  IO1CLR |= ((~(((unsigned int)dat)<<16))&((0XFF<<16)));
  IO0SET_bit.P0_12 = 1;IO0CLR_bit.P0_12 = 1;
}

//向液晶屏写入数据
void TFTWriData(unsigned short ch)
{
  LCD_RS_H();   //RS = 1
  LCD_CS_L();   //CS = 0
  Write_HC573(ch);
  LCD_WR_L();   //WR = 0
  LCD_WR_H();   //WR = 1
  LCD_CS_H();   //CS = 1
}

//向液晶屏写入命令
void TFTWriCom(unsigned short ch)
{
  LCD_RS_L();   //RS = 0
  Write_HC573(ch);
  LCD_WR_L();   //WR = 0
  LCD_CS_L();   //CS = 0
  LCD_WR_H();   //WR = 1
  LCD_CS_H();   //CS = 1
}

//写入控制字
void write_com(unsigned short address,unsigned short num)
{
    TFTWriCom(address);
    TFTWriData(num);
}

//液晶屏初始化
void TftInit(void)
{
    LCD_Port();
    CH375DataOut();
    LCD_RESET_L();      //低电平复位
    Delayn(10000);
    LCD_RESET_H();
#define delay Delayn
    write_com(0x0000,0x0001);    DelayNS(1000);  //打开晶振
    write_com(0x0003,0xA8A4);    DelayNS(1000);   //0xA8A4
    write_com(0x000C,0x0000);    DelayNS(1000);   
    write_com(0x000D,0x080C);    DelayNS(1000);   
    write_com(0x000E,0x2B00);    DelayNS(1000);   
    write_com(0x001E,0x00B0);    DelayNS(1000);   
    write_com(0x0001,0x2B3F);    DelayNS(1000);   //驱动输出控制320*240  0x6B3F
    write_com(0x0002,0x0600);    DelayNS(1000);
    write_com(0x0010,0x0000);    DelayNS(1000);
    write_com(0x0011,0x6070);    DelayNS(1000);    //0x4030  //定义数据格式  16位色 
    write_com(0x0005,0x0000);    DelayNS(1000);
    write_com(0x0006,0x0000);    DelayNS(1000);
    write_com(0x0016,0xEF1C);    DelayNS(1000);
    write_com(0x0017,0x0003);    DelayNS(1000);
    write_com(0x0007,0x0233);    DelayNS(1000);        //0x0233       
    write_com(0x000B,0x0000);    DelayNS(1000);
    write_com(0x000F,0x0000);    DelayNS(1000);        //扫描开始地址
    write_com(0x0041,0x0000);    DelayNS(1000);
    write_com(0x0042,0x0000);    DelayNS(1000);
    write_com(0x0048,0x0000);    DelayNS(1000);
    write_com(0x0049,0x013F);    DelayNS(1000);
    write_com(0x004A,0x0000);    DelayNS(1000);
    write_com(0x004B,0x0000);    DelayNS(1000);
    write_com(0x0044,0xEF00);    DelayNS(1000);
    write_com(0x0045,0x0000);    DelayNS(1000);
    write_com(0x0046,0x013F);    DelayNS(1000);
    write_com(0x0030,0x0707);    DelayNS(1000);
    write_com(0x0031,0x0204);    DelayNS(1000);
    write_com(0x0032,0x0204);    DelayNS(1000);
    write_com(0x0033,0x0502);    DelayNS(1000);
    write_com(0x0034,0x0507);    DelayNS(1000);
    write_com(0x0035,0x0204);    DelayNS(1000);
    write_com(0x0036,0x0204);    DelayNS(1000);
    write_com(0x0037,0x0502);    DelayNS(1000);
    write_com(0x003A,0x0302);    DelayNS(1000);
    write_com(0x003B,0x0302);    DelayNS(1000);
    write_com(0x0023,0x0000);    DelayNS(1000);
    write_com(0x0024,0x0000);    DelayNS(1000);
    write_com(0x0025,0x8000);    DelayNS(1000);
    write_com(0x004f,0);        //行首址0
    write_com(0x004e,0);        //列首址0
}

//显示一种颜色
void DispOneColor(unsigned short Color)
{
    unsigned char i; unsigned int j;
    write_com(0x004f,0);        //行首址0
    write_com(0x004e,0);        //列首址0
    TFTWriCom(0x22);
    LCD_RS_H();   //RS = 1
    LCD_CS_L();   //CS = 0
    Write_HC573(Color);
    for(j=0;j<320;j++)
        for(i=0;i<240;i++)
	{    
          LCD_WR_L();   //WR = 0
          LCD_WR_H();   //WR = 1
        }
    LCD_CS_H();   //CS = 1
}

//清屏
void DispClear(void)
{
    DispOneColor(BLACK);
}

//设置RAM地址
void Set_ramaddr(unsigned short x,unsigned short y)
{
    write_com(0x004f,y);        //行首址
    write_com(0x004e,x);        //列首址
    TFTWriCom(0x22); 
}

//显示一个小图片
void DispSmallPic(unsigned short x, unsigned short y, unsigned short w, unsigned short h, const unsigned char *str)
{
    unsigned short i,j,temp;
    for(j=0;j<h;j++)
    {
        Set_ramaddr(x,y+j);
        for(i=0;i<w;i++)
        {   //send_data(*(unsigned short *)(&str[(j*w+i)*2]));       //高位在前
            temp=str[(j*w+i)*2+1]<<8;     //低位在前
            temp|=str[(j*w+i)*2];
            TFTWriData(temp);
        }
    }
}

//显示一副240x320分辨率的图片
void DispPic240_320(const unsigned char *str)
{
    int i;
    Set_ramaddr(0,0);
    for (i = 0; i < 240 * 320; i++)
    {
        TFTWriData(*(unsigned short *)(&str[ 2 * i]));
    }
}

////////////////////////////////////////////////////////////////////////////
//画点
void LCD_SetPixel(unsigned short x, unsigned short y, unsigned short Color)
{
  Set_ramaddr(x,y);
  TFTWriData(Color);
}

//画线
void LCD_DrawLine(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2,unsigned short Color)
{
  int dy ;
  int dx ;
  int stepx, stepy, fraction;
  dy = y2 - y1;
  dx = x2 - x1;
  if (dy < 0) 
  {
    dy = -dy;
    stepy = -1;
  }
  else 
  {
    stepy = 1;
  }
  if (dx < 0)
  {
    dx = -dx;
    stepx = -1;
  }
  else
  {
    stepx = 1;
  }
  dy <<= 1;
  dx <<= 1;
  LCD_SetPixel(x1,y1,Color);
  if (dx > dy)
  {
    fraction = dy - (dx >> 1); 
    while (x1 != x2)
    {
      if (fraction >= 0)
      {
        y1 += stepy;
        fraction -= dx;
      }
      x1 += stepx;
      fraction += dy;  
      LCD_SetPixel(x1,y1,Color);
    }
  }
  else
  {
    fraction = dx - (dy >> 1);
    while (y1 != y2)
    {
        if (fraction >= 0)
        {
                x1 += stepx;
                fraction -= dy;
        }
        y1 += stepy;
        fraction += dx;
        LCD_SetPixel(x1,y1,Color);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////

int LCD_L0_Init(void)
{  
  TftInit();
  return 0;
}

unsigned int LCD_L0_GetPixelIndex(int x, int y)
{
  return 0;
}

void LCD_L0_XorPixel(int x, int y)
{
  LCD_SetPixel(x,y,BLACK);
}

void LCD_L0_SetPixelIndex(int x, int y, int PixelIndex)
{
  LCD_SetPixel(x,y,PixelIndex);
}

void LCD_L0_DrawHLine  (int x0, int y,  int x1)
{
  LCD_DrawLine(x0,y,x1,y,LCD_COLORINDEX);
}

void LCD_L0_DrawVLine  (int x, int y0,  int y1)
{
  LCD_DrawLine(x,y0,x,y1,LCD_COLORINDEX);
}


void DrawBitLine1BPP(int x, int y, unsigned char const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans)
{
  LCD_PIXELINDEX pixels;
  LCD_PIXELINDEX Index0 = *(pTrans+0);
  LCD_PIXELINDEX Index1 = *(pTrans+1);
/*
// Jump to right entry point
*/
  pixels = *p;
  switch (GUI_Context.DrawMode & (LCD_DRAWMODE_TRANS|LCD_DRAWMODE_XOR)) {
  case 0:
    #if defined (SETNEXTPIXEL)   /* Optimization ! */
      x+=Diff;
      Set_ramaddr(x,y);
    #endif
    switch (Diff&7) {
    case 0:   
      goto WriteBit0;
    case 1:   
      goto WriteBit1;
    case 2:
      goto WriteBit2;
    case 3:
      goto WriteBit3;
    case 4:
      goto WriteBit4;
    case 5:   
      goto WriteBit5;
    case 6:   
      goto WriteBit6;
    case 7:   
      goto WriteBit7;
    }
    break;
  case LCD_DRAWMODE_TRANS:
    switch (Diff&7) {
    case 0:
      goto WriteTBit0;
    case 1:
      goto WriteTBit1;
    case 2:
      goto WriteTBit2;
    case 3:
      goto WriteTBit3;
    case 4:
      goto WriteTBit4;
    case 5:   
      goto WriteTBit5;
    case 6:   
      goto WriteTBit6;
    case 7:   
      goto WriteTBit7;
    }
    break;
  case LCD_DRAWMODE_XOR:
    switch (Diff&7) {
    case 0:   
      goto WriteXBit0;
    case 1:   
      goto WriteXBit1;
    case 2:
      goto WriteXBit2;
    case 3:
      goto WriteXBit3;
    case 4:
      goto WriteXBit4;
    case 5:   
      goto WriteXBit5;
    case 6:   
      goto WriteXBit6;
    case 7:   
      goto WriteXBit7;
    }
  }
/*
        Write with transparency
*/

  WriteTBit0:
   if (pixels&(1<<7)) LCD_SetPixel(x+0, y, Index1);
    if (!--xsize)
      return;
  WriteTBit1:
    if (pixels&(1<<6)) LCD_SetPixel(x+1, y, Index1);
    if (!--xsize)
      return;
  WriteTBit2:
    if (pixels&(1<<5)) LCD_SetPixel(x+2, y, Index1);
    if (!--xsize)
      return;
  WriteTBit3:
    if (pixels&(1<<4)) LCD_SetPixel(x+3, y, Index1);
    if (!--xsize)
      return;
  WriteTBit4:
    if (pixels&(1<<3)) LCD_SetPixel(x+4, y, Index1);
    if (!--xsize)
      return;
  WriteTBit5:
    if (pixels&(1<<2)) LCD_SetPixel(x+5, y, Index1);
    if (!--xsize)
      return;
  WriteTBit6:
    if (pixels&(1<<1)) LCD_SetPixel(x+6, y, Index1);
    if (!--xsize)
      return;
  WriteTBit7:
    if (pixels&(1<<0)) LCD_SetPixel(x+7, y, Index1);
    if (!--xsize)
      return;
    x+=8;
    pixels = *(++p);
    goto WriteTBit0;

/*
        Write without transparency
*/

  WriteBit0:
    LCD_SetPixel(x+0, y, (pixels&(1<<7)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit1:
    LCD_SetPixel(x+1, y, (pixels&(1<<6)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit2:
    LCD_SetPixel(x+2, y, (pixels&(1<<5)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit3:
    LCD_SetPixel(x+3, y, (pixels&(1<<4)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit4:
    LCD_SetPixel(x+4, y, (pixels&(1<<3)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit5:
    LCD_SetPixel(x+5, y, (pixels&(1<<2)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit6:
    LCD_SetPixel(x+6, y, (pixels&(1<<1)) ? Index1 : Index0);
    if (!--xsize)
      return;
  WriteBit7:
    LCD_SetPixel(x+7, y, (pixels&(1<<0)) ? Index1 : Index0);
    if (!--xsize)
      return;
    x+=8;
    pixels = *(++p);
    goto WriteBit0;

/*
        Write XOR mode
*/

  WriteXBit0:
    if (pixels&(1<<7))
      LCD_L0_XorPixel(x+0, y);
    if (!--xsize)
      return;
  WriteXBit1:
    if (pixels&(1<<6))
      LCD_L0_XorPixel(x+1, y);
    if (!--xsize)
      return;
  WriteXBit2:
    if (pixels&(1<<5))
      LCD_L0_XorPixel(x+2, y);
    if (!--xsize)
      return;
  WriteXBit3:
    if (pixels&(1<<4))
      LCD_L0_XorPixel(x+3, y);
    if (!--xsize)
      return;
  WriteXBit4:
    if (pixels&(1<<3))
      LCD_L0_XorPixel(x+4, y);
    if (!--xsize)
      return;
  WriteXBit5:
    if (pixels&(1<<2))
      LCD_L0_XorPixel(x+5, y);
    if (!--xsize)
      return;
  WriteXBit6:
    if (pixels&(1<<1))
      LCD_L0_XorPixel(x+6, y);
    if (!--xsize)
      return;
  WriteXBit7:
    if (pixels&(1<<0))
      LCD_L0_XorPixel(x+7, y);
    if (!--xsize)
      return;
    x+=8;
    pixels = *(++p);
    goto WriteXBit0;
	
}

void LCD_L0_FillRect(int x0, int y0, int x1, int y1) {
#if !LCD_SWAP_XY
  for (; y0 <= y1; y0++) {
    LCD_L0_DrawHLine(x0,y0, x1);
  }
#else
  for (; x0 <= x1; x0++) {
    LCD_L0_DrawVLine(x0,y0, y1);
  }
#endif
}

//画图
void LCD_L0_DrawBitmap   (int x0, int y0,
                       int xsize, int ysize,
                       int BitsPerPixel, 
                       int BytesPerLine,
                       const unsigned char* pData, int Diff,
                       const LCD_PIXELINDEX* pTrans)
{
  int i;
  switch (BitsPerPixel)
  {
  case 1:
    for (i=0; i<ysize; i++)
    {
      DrawBitLine1BPP(x0, i+y0, pData, Diff, xsize, pTrans);
      pData += BytesPerLine;
    }
    break;
  case 2:
    for (i=0; i<ysize; i++)
    {
      DrawBitLine2BPP(x0, i+y0, pData, Diff, xsize, pTrans);
      pData += BytesPerLine;
    }
    break;
  case 4:
    for (i=0; i<ysize; i++)
    {
      DrawBitLine4BPP(x0, i+y0, pData, Diff, xsize, pTrans);
      pData += BytesPerLine;
    }
    break;
  case 8:
    for (i=0; i<ysize; i++)
    {
      DrawBitLine8BPP(x0, i+y0, pData, xsize, pTrans);
      pData += BytesPerLine;
    }
    break;
  case 16:
    for (i=0; i<ysize; i++)
    {
      DrawBitLine16BPP(x0, i+y0, (unsigned short*)pData, xsize);
      pData += BytesPerLine;
    }
    break;
  }
}

static void  DrawBitLine2BPP(int x, int y, U8 const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans) {
  LCD_PIXELINDEX pixels;
/*
// Jump to right entry point
*/
  pixels = *p;
  if (pTrans) {
    /*
      with palette
    */
    if (GUI_Context.DrawMode & LCD_DRAWMODE_TRANS) switch (Diff&3) {
    case 0:
      goto WriteTBit0;
    case 1:
      goto WriteTBit1;
    case 2:
      goto WriteTBit2;
    default:
      goto WriteTBit3;
    } else switch (Diff&3) {
    case 0:
      goto WriteBit0;
    case 1:
      goto WriteBit1;
    case 2:
      goto WriteBit2;
    default:
      goto WriteBit3;
    }
  /*
          Write without transparency
  */
  WriteBit0:
    LCD_SetPixel(x+0, y, *(pTrans+(pixels>>6)));
    if (!--xsize)
      return;
  WriteBit1:
    LCD_SetPixel(x+1, y, *(pTrans+(3&(pixels>>4))));
    if (!--xsize)
      return;
  WriteBit2:
    LCD_SetPixel(x+2, y, *(pTrans+(3&(pixels>>2))));
    if (!--xsize)
      return;
  WriteBit3:
    LCD_SetPixel(x+3, y, *(pTrans+(3&(pixels))));
    if (!--xsize)
      return;
    pixels = *(++p);
    x+=4;
    goto WriteBit0;
  /*
          Write with transparency
  */
  WriteTBit0:
    if (pixels&(3<<6))
      LCD_SetPixel(x+0, y, *(pTrans+(pixels>>6)));
    if (!--xsize)
      return;
  WriteTBit1:
    if (pixels&(3<<4))
      LCD_SetPixel(x+1, y, *(pTrans+(3&(pixels>>4))));
    if (!--xsize)
      return;
  WriteTBit2:
    if (pixels&(3<<2))
      LCD_SetPixel(x+2, y, *(pTrans+(3&(pixels>>2))));
    if (!--xsize)
      return;
  WriteTBit3:
    if (pixels&(3<<0))
      LCD_SetPixel(x+3, y, *(pTrans+(3&(pixels))));
    if (!--xsize)
      return;
    pixels = *(++p);
    x+=4;
    goto WriteTBit0;
  } else { 
    /* 
      without palette 
    */
    if (GUI_Context.DrawMode & LCD_DRAWMODE_TRANS) switch (Diff&3) {
    case 0:
      goto WriteDDPTBit0;
    case 1:
      goto WriteDDPTBit1;
    case 2:
      goto WriteDDPTBit2;
    default:
      goto WriteDDPTBit3;
    } else switch (Diff&3) {
    case 0:
      goto WriteDDPBit0;
    case 1:
      goto WriteDDPBit1;
    case 2:
      goto WriteDDPBit2;
    default:
      goto WriteDDPBit3;
    }
  /*
          Write without transparency
  */
  WriteDDPBit0:
    LCD_SetPixel(x+0, y, (pixels>>6));
    if (!--xsize)
      return;
  WriteDDPBit1:
    LCD_SetPixel(x+1, y, (3&(pixels>>4)));
    if (!--xsize)
      return;
  WriteDDPBit2:
    LCD_SetPixel(x+2, y, (3&(pixels>>2)));
    if (!--xsize)
      return;
  WriteDDPBit3:
    LCD_SetPixel(x+3, y, (3&(pixels)));
    if (!--xsize)
      return;
    pixels = *(++p);
    x+=4;
    goto WriteDDPBit0;
  /*
          Write with transparency
  */
  WriteDDPTBit0:
    if (pixels&(3<<6))
      LCD_SetPixel(x+0, y, (pixels>>6));
    if (!--xsize)
      return;
  WriteDDPTBit1:
    if (pixels&(3<<4))
      LCD_SetPixel(x+1, y, (3&(pixels>>4)));
    if (!--xsize)
      return;
  WriteDDPTBit2:
    if (pixels&(3<<2))
      LCD_SetPixel(x+2, y, (3&(pixels>>2)));
    if (!--xsize)
      return;
  WriteDDPTBit3:
    if (pixels&(3<<0))
      LCD_SetPixel(x+3, y, (3&(pixels)));
    if (!--xsize)
      return;
    pixels = *(++p);
    x+=4;
    goto WriteDDPTBit0;
  }
}

static void  DrawBitLine4BPP(int x, int y, U8 const*p, int Diff, int xsize, const LCD_PIXELINDEX*pTrans)
{
  LCD_PIXELINDEX pixels;

  pixels = *p;
  if (pTrans)
  {
    if (GUI_Context.DrawMode & LCD_DRAWMODE_TRANS)
    {
      if ((Diff&1) ==0)
        goto WriteTBit0;
        goto WriteTBit1;
    }
    else
    {
      if ((Diff&1) ==0)
        goto WriteBit0;
        goto WriteBit1;
    }

  WriteBit0:
    LCD_SetPixel(x+0, y, *(pTrans+(pixels>>4)));
    if (!--xsize)
      return;
  WriteBit1:
    LCD_SetPixel(x+1, y, *(pTrans+(pixels&0xf)));
    if (!--xsize)
      return;
    x+=2;
    pixels = *(++p);
    goto WriteBit0;
  /*
          Write with transparency
  */
  WriteTBit0:
    if (pixels>>4)
      LCD_SetPixel(x+0, y, *(pTrans+(pixels>>4)));
    if (!--xsize)
      return;
  WriteTBit1:
    if (pixels&0xf)
      LCD_SetPixel(x+1, y, *(pTrans+(pixels&0xf)));
    if (!--xsize)
      return;
    x+=2;
    pixels = *(++p);
    goto WriteTBit0;
  } else {
    /*
      without palette
    */
    if (GUI_Context.DrawMode & LCD_DRAWMODE_TRANS) {
      if ((Diff&1) ==0)
        goto WriteDDPTBit0;
      goto WriteDDPTBit1;
    } else {
      if ((Diff&1) ==0)
        goto WriteDDPBit0;
      goto WriteDDPBit1;
    }
  /*
          Write without transparency
  */
  WriteDDPBit0:
    LCD_SetPixel(x+0, y, (pixels>>4));
    if (!--xsize)
      return;
  WriteDDPBit1:
    LCD_SetPixel(x+1, y, (pixels&0xf));
    if (!--xsize)
      return;
    x+=2;
    pixels = *(++p);
    goto WriteDDPBit0;
  /*
          Write with transparency
  */
  WriteDDPTBit0:
    if (pixels>>4)
      LCD_SetPixel(x+0, y, (pixels>>4));
    if (!--xsize)
      return;
  WriteDDPTBit1:
    if (pixels&0xf)
      LCD_SetPixel(x+1, y, (pixels&0xf));
    if (!--xsize)
      return;
    x+=2;
    pixels = *(++p);
    goto WriteDDPTBit0;
  }
}

void DrawBitLine8BPP(int x, int y, U8 const*p, int xsize, const LCD_PIXELINDEX*pTrans) {
  LCD_PIXELINDEX pixel;
  if ((GUI_Context.DrawMode & LCD_DRAWMODE_TRANS)==0) {
    if (pTrans) {
      for (;xsize > 0; xsize--,x++,p++) {
        pixel = *p;
        LCD_SetPixel(x, y, *(pTrans+pixel));
      }
    } else {
      for (;xsize > 0; xsize--,x++,p++) {
        LCD_SetPixel(x, y, *p);
      }
    }
  } else {   /* Handle transparent bitmap */
    if (pTrans) {
      for (; xsize > 0; xsize--, x++, p++) {
        pixel = *p;
        if (pixel) {
          LCD_SetPixel(x+0, y, *(pTrans+pixel));
        }
      }
    } else {
      for (; xsize > 0; xsize--, x++, p++) {
        pixel = *p;
        if (pixel) {
          LCD_SetPixel(x+0, y, pixel);
        }
      }
    }
  }
}

void DrawBitLine16BPP(int x, int y, U16 const*p, int xsize)
{
  LCD_PIXELINDEX Index;
  if ((GUI_Context.DrawMode & LCD_DRAWMODE_TRANS)==0)
  {
    for (;xsize > 0; xsize--,x++,p++)
    {
      LCD_SetPixel(x, y, *p);
    }
  }
  else
  {   /* Handle transparent bitmap */
    for (; xsize > 0; xsize--, x++, p++)
    {
      Index = *p;
      if (Index)
      {
        LCD_SetPixel(x+0, y, Index);
      }
    }
  }
}

void LCD_L0_SetLUTEntry(U8 Pos, LCD_COLOR color){}
void LCD_L0_On(void){}