#ifndef _HC595_H_
#define _HC595_H_

#define         SCLK  0x01<<24
#define         SCK0  0x01<<4 
#define         MISO  0x01<<5
#define         MOSI  0x01<<6
#define         RCK   0x01<<7

#define Init_165() IO1DIR_bit.P1_25 = 1; IO1SET_bit.P1_25 = 1
extern unsigned int HC595_DATA;
void Delayn(unsigned long n);
void HC595_Init(void);
void Write595(void);

#endif