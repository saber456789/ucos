#ifndef _TFT_LCD_H_
#define _TFT_LCD_H_

/* 系统设置, Fosc、Fcclk、Fcco、Fpclk必须定义*/
/*
#define Fosc			12000000				//晶振频率,10MHz~25MHz，应当与实际一至
#define Fcclk   		(Fosc * 5)  			       	//系统频率，必须为Fosc的整数倍(1~32)，且<=60MHZ
#define Fcco			(Fcclk * 2) 				//CCO频率，必须为Fcclk的2、4、8、16倍，范围为156MHz~320MHz
#define Fpclk   		(Fcclk / 4) * 4 			//VPB时钟频率，只能为(Fcclk / 4)的1 ~ 4倍
*/
#define LCD_CALIBRATE (1) //打开液晶屏校验
//液晶屏接口宏定义
#define RED  	0xf800
#define GREEN	0x07e0
#define BLUE 	0x001f
#define WHITE	0xffff
#define BLACK	0x0000
#define YELLOW  0xFFE0
#define CH375DataOut() IO1DIR |= (0XFF<<16) //D0~D7
#define CH375DataIn() IO1DIR &= ~(0XFF<<16)   //D0~D7
#define LCD_RS_H() IO0SET_bit.P0_30 = 1
#define LCD_RS_L() IO0CLR_bit.P0_30 = 1
#define LCD_WR_H() IO0SET_bit.P0_21 = 1
#define LCD_WR_L() IO0CLR_bit.P0_21 = 1
#define LCD_RD_H() IO0SET_bit.P0_20 = 1
#define LCD_RD_L() IO0CLR_bit.P0_20 = 1
#define LCD_CS_L() IO0CLR_bit.P0_17 = 1
#define LCD_CS_H() IO0SET_bit.P0_17 = 1
#define LCD_RESET_L() HC595_DATA &= ~(1<<7);Write595()
#define LCD_RESET_H() HC595_DATA |= (1<<7) ;Write595()


//函数声明
void LCD_Port(void);
void TftInit(void);
void DispOneColor(unsigned short Color);
void TFTWriCom(unsigned short ch);
void DispSmallPic(unsigned short x, unsigned short y, unsigned short w, unsigned short h, const unsigned char *str);
void DispPic240_320(const unsigned char *str);
void LCD_DrawLine(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2,unsigned short Color);
void LCD_SetPixel(unsigned short x, unsigned short y, unsigned short Color);

void PLL_Init(void);
#endif 