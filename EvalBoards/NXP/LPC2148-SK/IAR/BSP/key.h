#ifndef _KEY_H_
#define _KEY_H_

//函数声明
extern void DataDisp(unsigned char data,unsigned char n);
extern void HC165_CS(char flag);
extern unsigned char Read165(void);
extern void KeyDown(void);
extern void KeyInteruptInit(void callback(void));
//595扩展接口的数据显示缓存
extern const unsigned char  SEGMENT_DATA[16];
extern unsigned char HC165_DATA;
extern char networkenable;
#endif