/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : key              
  File Name     : key.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : 1x4按键实验                         
  Description  	: 无                 
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include <NXP/iolpc2148.h>
#include "includes.h"
//数码管表
const unsigned char  SEGMENT_DATA[16] = {
  0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,
  0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71 };
unsigned char HC165_DATA;
char networkenable;
//主函数
void KeyDown(void)
{
  uint32 FlashWrite[10]={0,1,2};
  uint8  FlashRead[10]={0};
  HC165_DATA = Read165();
  if((HC165_DATA&(1<<6))==0)
  {
    DataDisp(4,3);
    SelectSector(26,26);
    EraseSector(26,26);
    //BEEPON();
  }
  else if((HC165_DATA&(1<<5))==0)
  {
    DataDisp(3,3);
    SelectSector(26,26);
    RamtoFlash(0x0007C000,FlashWrite,256);
    //BEEPON();
  }
  else if((HC165_DATA&(1<<4))==0)
  {
    DataDisp(2,3);
    Flash_Read(0x0007C000,FlashRead,4);
    networkenable = 0;
    //BEEPON();
  }
  else if((HC165_DATA&(1<<3))==0)
  {
    DataDisp(1,3);
    networkenable = 1;
    //BEEPON();
  }
  EXTINT_bit.EINT1 = 1;                                                         //清中断标志
}

//数据显示
void DataDisp(unsigned char data,unsigned char n)
{
  if(data<16 && n<4)
  {
    HC595_DATA &=0xFFFFF;
    HC595_DATA |= (SEGMENT_DATA[data]<<24);
    HC595_DATA |= ((~((((unsigned char)1))<<(n+20)))&0xF00000);
    Write595();
  }
}

//165片选
void HC165_CS(char flag)
{
  if(flag)
    HC595_DATA &= ~(1<<1);
  else
    HC595_DATA |= (1<<1);
  Write595();
}
//读165的数据
unsigned char Read165(void)
{
  unsigned char RD=0,i;
  HC165_CS(1);
  IO1CLR_bit.P1_25 = 1; 
  Delayn(10);
  IO1SET_bit.P1_25 = 1;
  for(i=0;i<8;i++)
  {
    RD <<= 1; 
    if(IO0PIN&MISO)   
      RD |= 1;  
    IO0SET=SCK0;
    Delayn(10);
    IO0CLR=SCK0; 
  } 
  HC165_CS(0);
  return RD;
}

void KeyInteruptInit(void callback(void))
{
  VICIntSelect &= ~(1<<VIC_EINT1);
  VICVectAddr11 = (CPU_INT32U)callback;
  PINSEL0_bit.P0_14 = 0x2;
  EXTMODE |= 0x02;                                                               //边沿触发
  EXTPOLAR |= 0x00;                                                              //下降沿触发
  VICVectCntl11 = 0x20|VIC_EINT1;                                               //20为使能优先级配置
  EXTINT_bit.EINT1 = 1;
  VICIntEnable  = (1<<VIC_EINT1);
    
}
