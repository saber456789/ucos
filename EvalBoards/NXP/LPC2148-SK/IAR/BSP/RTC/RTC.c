/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : RTC              
  File Name     : RTC.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : 片内实时时钟实验                        
  Description  	: 每一秒蜂鸣器发声，同时串口输出当前的时间           
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include "lpc_rtc.h"

//函数声明
void Delayn(unsigned long n); 

LPC_Rtc_Date_t CT=
{
  .year  = 2017,
  .month = 5,
  .day   = 3,
  .DOW   = 4,
  .DOY   = 122
};
LPC_Rtc_Time_t CTI={
  .hour = 17,
  .minute = 49,
  .second = 50
};
//主函数
