#ifndef _BEEP_H_
#define _BEEP_H_
#include "includes.h"

#define BEEPIN P0_22
#define BEEPON()  IO0SET_bit.BEEPIN = 1
#define BEEPOFF() IO0CLR |= 0x01<<22;

#endif