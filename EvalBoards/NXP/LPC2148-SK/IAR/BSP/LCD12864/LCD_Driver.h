#ifndef _LCDDriver_H_
#define _LCDDriver_H_

#include "config.h"

void Draw_Line(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2,unsigned char flag);
void  LCD12864_Initialize(void);
void  LCD12864_FillSCR(unsigned char dat);
void  LCD12864_ClearSCR(void);
void  LCD12864_HLine(unsigned int x0, unsigned int y0, unsigned int x1, unsigned char color);
void  LCD12864_RLine(unsigned int x0, unsigned int y0, unsigned int y1, unsigned char color);
int   LCD12864_ReadPoint(unsigned int x, unsigned int y, unsigned char *ret);
unsigned char  LCD12864_Point(unsigned int x, unsigned int y, unsigned char color);
void LCD12864_DispHZ(uint16 usX, uint16 usY, uint16 usForeColor,  uint8 *pucStr);
int LCD12864_Putc(uint8 x,uint8 y,char C,uint8 flag);
void LCD12864_Puts(uint8 x,uint8 y,char *str,uint8 flag);
/****************************************************************************
* 名矻CD12864_UI_CmpColor()
* 功能：判断颜色值是否一致。
* 入口参数：color1		颜色值1
*		   color2		颜色值2
* 出口参数：返回1表示相同，返回0表示不相同。
* 说明：由于颜色类型TCOLOR可以是结构类型，所以需要用户编写比较函数。
****************************************************************************/
//extern int  LCD12864_CmpColor(TCOLOR color1, TCOLOR color2);
#define  LCD12864_CmpColor(color1, color2)	(color1==color2)

/****************************************************************************
* 名矻CD12864_UI_CopyColor()
* 功能：颜色值复制。
* 入口参数：color1		目标颜色变量
*		   color2		源颜色变量
* 出口参数：无
* 说明：由于颜色类型TCOLOR可以是结构类型，所以需要用户编写复制函数。
****************************************************************************/
//extern void  LCD12864_CopyColor(TCOLOR *color1, TCOLOR color2);
#define  LCD12864_CopyColor(color1, color2) 	*color1 = color2
extern uint8 const ICO1[];

#endif