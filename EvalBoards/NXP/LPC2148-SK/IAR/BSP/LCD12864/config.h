#ifndef _CONFIG_H_
#define _CONFIG_H_


//GUI Define
// 定义颜色数据类型(可以是数据结构)
#define  TCOLOR			uint8

#define  LCD12864_LCM_XMAX		128	// 定义液晶x轴的点数 
#define  LCD12864_LCM_YMAX		64	// 定义液晶y轴的点数 

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL  0
#endif

typedef unsigned char  uint8;                   /* 无符号8位整型变量                        */
typedef signed   char  int8;                    /* 有符号8位整型变量                        */
typedef unsigned short uint16;                  /* 无符号16位整型变量                       */
typedef signed   short int16;                   /* 有符号16位整型变量                       */
typedef unsigned int   uint32;                  /* 无符号32位整型变量                       */
typedef signed   int   int32;                   /* 有符号32位整型变量                       */
typedef float          fp32;                    /* 单精度浮点数（32位长度）                 */
typedef double         fp64;                    /* 双精度浮点数（64位长度）                 */


#endif