/******************************************************************
  Copyright (C), 2008-2009, 力天电子，LiTian Tech.Co.Ltd.
  Module Name   : LT_ARM214X_TEST_ALL              
  File Name     : pll.c	            
  Author   	: HE CHENG         
  Create Date  	: 2009/10/19         
  Version   	: 1.0                
  Function      : 系统时钟设置                       
  Description  	: 无     
  Support     	: www.LT430.com  QQ:330508762                  
******************************************************************/
#include <nxp/iolpc2148.h>

//系统时钟
#define Fosc        12000000UL        //晶振时钟 【Hz】
#define Fcclk       (Fosc * 5)        //系统频率，必须为Fosc的整数倍(1~32)，且<=60MHZ
#define Fcco	    (Fcclk * 4)	      //CCO频率，必须为Fcclk的2、4、8、16倍，范围为156MHz~320MHz
#define Fpclk       (Fcclk / 4) * 4   //VPB时钟频率，只能为(Fcclk / 4)的1 ~ 4倍

/*PLL初始化*/
void PLL_Init(void)
{
    /* 设置系统各部分时钟 */
       PLLCON = 1;
    #if ((Fcclk / 4) / Fpclk) == 1
            VPBDIV = 0;
    #endif
    #if ((Fcclk / 4) / Fpclk) == 2
            VPBDIV = 2;
    #endif
    #if ((Fcclk / 4) / Fpclk) == 4
            VPBDIV = 1;
    #endif
    #if (Fcco / Fcclk) == 2
            PLLCFG = ((Fcclk / Fosc) - 1) | (0 << 5);
    #endif
    #if (Fcco / Fcclk) == 4
            PLLCFG = ((Fcclk / Fosc) - 1) | (1 << 5);
    #endif
    #if (Fcco / Fcclk) == 8
            PLLCFG = ((Fcclk / Fosc) - 1) | (2 << 5);
    #endif
    #if (Fcco / Fcclk) == 16
            PLLCFG = ((Fcclk / Fosc) - 1) | (3 << 5);
    #endif
            PLLFEED = 0xaa;
            PLLFEED = 0x55;
            while((PLLSTAT & (1 << 10)) == 0);
            PLLCON = 3;
            PLLFEED = 0xaa;
            PLLFEED = 0x55;	
}