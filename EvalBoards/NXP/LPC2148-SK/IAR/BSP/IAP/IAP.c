#include <NXP/iolpc2148.h>
#include "includes.h"
#include "IAP.h"
/*
*sample :     select = SelectSector(26,26);
              erease = EraseSector(26,26);
              select = SelectSector(26,26);
              copyresult = RamtoFlash(0x0007C000,FlashWrite,256);
              Flash_Read(0x0007C000,FlashRead,4);
*/

unsigned long command[5]={0};
unsigned long result[2]={0};

int SelectSector(uint8 start,uint8 end)
{
  unsigned int result,para[3];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = IAPrepare;
  para[1] = start;
  para[2] = end;
  iap_entry(para,&result);
  
  return result;
}

int RamtoFlash(uint32 dst,uint32 * src,uint32 size)
{
#if OS_CRITICAL_METHOD == 3                                     /* Allocate storage for CPU status register                 */
  OS_CPU_SR   cpu_sr = 0;
#endif
  OS_ENTER_CRITICAL();
  uint32 result,para[5];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = RAMTOFLASH;
  para[1] = dst;
  para[2] = (uint32)src;
  para[3] = size;
  para[4] = 48000;                                                              //CCLK  ��λΪKhz
  iap_entry(para,&result);
  OS_EXIT_CRITICAL();
  
  return result;
}

int EraseSector(uint8 start,uint8 end)
{
#if OS_CRITICAL_METHOD == 3                                     /* Allocate storage for CPU status register                 */
  OS_CPU_SR   cpu_sr = 0;
#endif
  OS_ENTER_CRITICAL();
  unsigned int result,para[4];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = EREASRSECTOR;
  para[1] = start;
  para[2] = end;
  para[3] = 48000;
  iap_entry(para,&result);
  OS_EXIT_CRITICAL();
  return result;
}

int BlankCheck(uint8 start,uint8 end)
{
  unsigned int result,para[3];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = SECTORBLANKCHK;
  para[1] = start;
  para[2] = end;
  
  iap_entry(para,&result);
  return result;
}

int Flash_Read(uint32 iAddress, uint8 *buf, int32 iNbrToRead)
{
    int i = 0;
    while(i< iNbrToRead)
    {
        *(buf+ i)=*(uint8*) iAddress++;
        i++;
    }
    return i;
}

//������id
int ReadID()
{
  unsigned int result,para[1];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = READPARTID;
  iap_entry(para,&result);
  
  return result;
}

int BootVersion()
{
  unsigned int result,para[1];
  IAP iap_entry;
  iap_entry=(IAP) IAP_LOCATION;
  
  para[0] = READBOOTVERSION;
  iap_entry(para,&result);
  
  return result;  
}