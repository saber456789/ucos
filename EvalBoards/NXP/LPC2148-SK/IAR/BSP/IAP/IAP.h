#ifndef _IAP_H_
#define _IAP_H_
#include "includes.h"

#define  IAP_LOCATION 0x7ffffff1
#define CMD_SUCCESS                                      0
#define INVALID_COMMAND                                  1
#define SRC_ADDR_ERROR                                   2
#define DST_ADDR_ERROR                                   3
#define SRC_ADDR_NOT_MAPPED                              4
#define DST_ADDR_NOT_MAPPED                              5
#define COUNT_ERROR                                      6
#define INVALID_SECTOR                                   7
#define SECTOR_NOT_BLANK                                 8
#define SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION          9
#define COMPARE_ERROR                                   10
#define BUSY                                            11

#define IAPrepare               50
#define RAMTOFLASH              51
#define EREASRSECTOR            52
#define SECTORBLANKCHK          53
#define READPARTID              54
#define READBOOTVERSION         55
#define COMPARE                 56

extern unsigned long command[5];
extern unsigned long result[2];
typedef void (*IAP)(unsigned int [],unsigned int[]);
extern int EraseSector(uint8 start,uint8 end);
extern int RamtoFlash(uint32 dst,uint32 *src,uint32 size);
extern int SelectSector(uint8 start,uint8 end);
extern int Flash_Read(uint32 iAddress, uint8 *buf, int32 iNbrToRead);

#endif