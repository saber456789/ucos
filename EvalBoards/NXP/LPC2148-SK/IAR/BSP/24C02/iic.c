#include "includes.h"
#include "iic.h"

unsigned char I2C_buf1[10];
unsigned char I2C_buf2[10];
unsigned char i;

/***********************i2c总线初始化*********************/
void  I2C_Init(void)
{  /* 设置I2C时钟为100KHz */
   I2C0SCLH = I2C0SCLL = 15;          // 晶振为12MHz，Fpclk = 3MHz,I2C时钟速率=3M/30=100k
   I2C0CONCLR = STA|SI|AA|STO;
   I2C0CONSET = I2CEN;                  //I2EN=1,使能I2C功能
}

/*************************发送起始信号*********************/
void I2C_Start(void)
{
  I2C0CONSET = STA;                    //启动发送START脉冲
//I2CONSET=SI;                     //发送完起始位会自动置1
  do{}while(0x08!=I2C0STAT);        //等待完成start脉冲发送
  I2C0CONCLR = STA;                    //清除STA位,结束过程
}

/*****************发送结束信号*********************/
void I2C_Stop(void)
{
  I2C0CONSET = STO;                    //启动发送STop脉冲,STO位是自动清除的.不必判断是否结束
  I2C0CONCLR = SI;                     //只能清除SI，清除STO还是会有影响，STO同时清除，返回到初始化的状态
}

/************************延时*********************************/
void  DelayMs(unsigned int  dly)
{  unsigned int  i;

   for(; dly>0; dly--)
      for(i=0; i<1000; i++);
}

/***************写8位数据*****************************/
void WriteByte_24C02(unsigned char data)
{
  I2C0DAT = data;
  I2C0CONCLR = SI;                     //启动发送
}

/***************写器件地址和读写信号*****************************/
void WriteAddr(unsigned char ReadMode)
{
  WriteByte_24C02(C02MasterAddr+ReadMode);
  if(ReadMode)  do{}while(I2C0STAT!=0x40); //等待器件地址和读信号完成
  else   do{}while(I2C0STAT!=0x18);        //等待器件地址和写信号完成
}


/***************写数据*****************************/
void WriteData_24C02(unsigned char data)
{
  WriteByte_24C02(data);
  do{}   while(I2C0STAT!=0x28);            //等待写数据完成
}

/***************读8位数据*****************************/
unsigned char ReadByte(unsigned char last)                       //last：最后一位数据
{
  if(last)
  {
    I2C0CONCLR=AA;
    I2C0CONCLR=SI;                   //启动接收最后数据
    do{}while(I2C0STAT!=0X58);
  }
  else
  {
    I2C0CONSET=AA;
    I2C0CONCLR=SI;                   //启动接收数据
    do{}while(I2C0STAT!=0X50);
  }
  return I2C0DAT;
}

/***************写4个字节到24c02*****************************/
void WriteC02()
{
  unsigned char i;
  I2C_Start();
  WriteAddr(WRMode);                   //写地址和写模式
  WriteData_24C02(C02SlaveAddr);             //24c02的存储地址
  for(i=0;i<4;i++)  I2C_buf1[i]=i+5;   //I2C_buf初始化
  for(i=0;i<4;i++)
  {
    WriteData_24C02(I2C_buf1[i]);
  }
  I2C_Stop();
  DelayMs(10);                       // 延时等待eeprom写操作
  //把写到I2C上的数据在串口上打印出来
  DelayMs(100);
  sendStr(0," \nWrite to I2C BUS:\n");
  for(i=0;i<4;i++)
  {
    sendByte(0,I2C_buf1[i]+48);
    sendStr(0,"\n");
  }
}
/******************从24c02地址读4个字节数据************/
void ReadC02()
{
  unsigned char i;
  I2C_Start();
  WriteAddr(WRMode);
  WriteData_24C02(C02SlaveAddr);
  I2C_Stop();

  I2C_Start();
  WriteAddr(RDMode);
  for(i=0;i<4;i++) I2C_buf2[i]=0;
  for(i=0;i<3;i++)
  {
    I2C_buf2[i]=ReadByte(!lastbyte);
  }
  I2C_buf2[3]=ReadByte(lastbyte);
  I2C_Stop();
  //把从I2C上读到的数据在串口上打印出来
  sendStr(0," \nRead form I2C BUS:\n");
  for(i=0;i<4;i++)
  {
    sendByte(0,I2C_buf2[i]+48);
    sendStr(0,"\n");
  }
}

/*************************主函数*********************/
void eeprom_main()
{
  PINSEL0_bit.P0_2 = 1;			// 设置I/O口工作模式，使用I2C0口,P0.2,P0.3和UART0的P0.0,P0.1。其余口为GPIO口
  PINSEL0_bit.P0_3 = 1;
  
  I2C_Init();                           //I2C初始化
  WriteC02();                         //往24C02写4个字节数据
  DelayMs(100);                      //延时
  ReadC02();                          //从24C02把写进去的数读出来
  DelayMs(100);                      //延时
  
/*********判断是否相等，并显示***************/
  for(i=0;i<4;i++)
  {
    if(I2C_buf2[i]!=I2C_buf1[i])        //如果有不相等的出现
      break;
  }
  if(i==4)  sendStr(0,"Right!\n");
  else      sendStr(0,"Wrong!\n");
  
  while(1);
}