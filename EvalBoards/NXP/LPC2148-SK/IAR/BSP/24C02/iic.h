#ifndef _IIC_H_
#define _IIC_H_

#include "includes.h"
#define STA   (1<<5)
#define I2CEN (1<<6)
#define STO   (1<<4)
#define SI    (1<<3)
#define AA    (1<<2)

#define   WRMode     0                               //写控制位
#define   RDMode     1                               //读控制位
#define   C02MasterAddr  0xA0                        //24c02的器件地址
#define   C02SlaveAddr    0                          //24C02的子地址
#define   lastbyte   1                               //最后一位标志位

#endif